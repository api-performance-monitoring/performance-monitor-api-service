# Implementation for the Performance Monitor API
___
This project is incompatible with JDK9. Only works on from JDK6 to JDK8.

There is a REST and JMS implementation available. 

It supports MySQL and SQL Server.  

## Setting up the environment
### Using MySQL 
 Ensure to edit the environment.properties file
```
mybatis.configpath=mybatis-mysql-config.xml
```
Ensure to edit the flyway.mysql.properties file
execute the following mvn command to setup the database:
```
flyway:migrate -P flyway-migrate -Dflyway.configFile=flyway.mysql.properties
```
Ensure to edit the mybatis-mysql-config.xml
### Using SQL Server 
 Ensure to edit the environment.properties file
```
mybatis.configpath=mybatis-sqlserver-config.xml
```
Ensure to edit the flyway.mssql.properties file
execute the following mvn command to setup the database:
```
flyway:migrate -P flyway-migrate -Dflyway.configFile=flyway.mssql.properties
```
Ensure to edit the mybatis-sqlserver-config.xml

## Running Locally
Copy the War into any Available Wildfly 8/10 Server into the $SERVER_HOME$/standalone/deployments folder. 

Make sure that the appropriate mybatis-mysql-config.xml or mybatis-sqlserver-config.xml is included in your war.
By default the one out of /src/main/resources is included.

If you are using Wildfly 8 configure the application datasource like so: [https://docs.jboss.org/author/display/WFLY8/DataSource+configuration](https://docs.jboss.org/author/display/WFLY8/DataSource+configuration)
If you are using Wildfly 9 configure the application datasource like so: [https://docs.jboss.org/author/display/WFLY9/DataSource+configuration](https://docs.jboss.org/author/display/WFLY9/DataSource+configuration)
If you are using Wildfly 10 configure the application datasource like so: [https://docs.jboss.org/author/display/WFLY10/DataSource+configuration](https://docs.jboss.org/author/display/WFLY10/DataSource+configuration)

Ensure to register a Datasource with a JNDI name of: java:jboss/datasources/PerformanceReportingDS
This should be configured to your database (either MySQL or MSSQL).

Example For MySQL:
```
<datasource jndi-name="java:jboss/datasources/PerformanceReportingDS" pool-name="PerformanceReportingDS">
    <connection-url>jdbc:mysql://localhost:3306/performance_reporting</connection-url>
    <driver>mysql</driver>
    <pool>
        <min-pool-size>10</min-pool-size>
        <max-pool-size>20</max-pool-size>
        <prefill>true</prefill>
    </pool>
    <security>
        <user-name>root</user-name>
        <password>root</password>
    </security>
</datasource>
```
Start your server by executing the following:
```
./standalone.sh -c standalone-full.xml -b 0.0.0.0
```
OR
```
standalone.bat -c standalone-full.xml -b 0.0.0.0
```
Once that is done and your wildfly server is running (assuming its setup as a default server) There is a swagger document available on: [localhost:8080/performancemonitor/api_specification](http://localhost:8080/performancemonitor/api_specification/)

You can then start to submit requests from your own API using: 
[localhost:8080/performancemonitor/v1/ws/monitor/request/add](http://localhost:8080/performancemonitor/v1/ws/monitor/request/add)
[localhost:8080/performancemonitor/v1/ws/monitor/request/add/exception](http://localhost:8080/performancemonitor/v1/ws/monitor/request/add/exception)

A tool like Postman for chrome or desktop is highly useful for this if the Swagger documentation is not sufficient.