CREATE TABLE [external_api] (
  [ExternalAPI_PK] INT NOT NULL IDENTITY(1,1),
  [ApplicationID] VARCHAR(32) NOT NULL,
  [ApplicationName] VARCHAR(256) NOT NULL,
  [ActiveFlag] BIT NOT NULL,
  [CreatedBy] VARCHAR(128) NOT NULL,
  [CreatedTime] DATETIME NOT NULL,
  [UpdatedBy] VARCHAR(128) NOT NULL,
  [UpdatedTime] DATETIME NOT NULL,
  CONSTRAINT [PK_external_api] PRIMARY KEY CLUSTERED (
	[ExternalAPI_PK] ASC
	)
)

CREATE TABLE [api_request] (
  [APIRequest_PK] INT NOT NULL IDENTITY(1,1),
  [ExternalAPI_FK] INT NOT NULL,
  [RequestID] VARCHAR(64) NOT NULL,-- Unique from external API
  [RequestUser] VARCHAR(256) NOT NULL,
  [RequestStartTime] DATETIME NOT NULL,
  [RequestEndTime] DATETIME NOT NULL,
  CONSTRAINT [PK_api_request] PRIMARY KEY CLUSTERED (
	[APIRequest_PK] ASC
	),
	CONSTRAINT [unique_api_request] UNIQUE NONCLUSTERED ([ExternalAPI_FK] ASC, [RequestID] ASC),
	CONSTRAINT [api_request__external_api] FOREIGN KEY ([ExternalAPI_FK]) REFERENCES [external_api] ([ExternalAPI_PK])
)

CREATE TABLE [api_request_detail] (
  [RequestDetail_PK] INT NOT NULL IDENTITY(1,1),
  [APIRequest_FK] INT NOT NULL,
  [WebMethodName] VARCHAR(256) NOT NULL,
  [WebMethodParameters] VARCHAR(2048) NULL,
  [RequestSenderIPAddress] VARCHAR(64) NOT NULL,
  [RequestProcessorIPAddress] VARCHAR(64) NOT NULL,
  [HTTPResponseCode] INT NOT NULL,
  [RequestSize] INT NOT NULL,
  [ResponseSize] INT NOT NULL,
  CONSTRAINT [PK_api_request_detail] PRIMARY KEY CLUSTERED (
	[RequestDetail_PK] ASC
	),
  CONSTRAINT [api_request_detail__api_request] FOREIGN KEY ([APIRequest_FK]) REFERENCES [api_request] ([APIRequest_PK])
)

CREATE TABLE [api_request_detail_exception] (
  [RequestException_PK] INT NOT NULL IDENTITY(1,1),
  [APIRequest_FK] INT NOT NULL,
  [ExceptionMessage] VARCHAR(1024) NOT NULL,
  [ClassName] VARCHAR(256) NULL, 
  [ErrorLineNumber] INT NULL,
  CONSTRAINT [PK_api_request_detail_exception] PRIMARY KEY CLUSTERED (
	[RequestException_PK] ASC
	),
	CONSTRAINT [api_request_detail_exception__api_request] FOREIGN KEY ([APIRequest_FK]) REFERENCES [api_request] ([APIRequest_PK])
)