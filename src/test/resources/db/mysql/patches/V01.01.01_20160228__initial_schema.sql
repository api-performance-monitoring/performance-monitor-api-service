CREATE TABLE `external_api` (
  `ExternalAPI_PK` INT NOT NULL AUTO_INCREMENT,
  `ApplicationID` VARCHAR(32) NOT NULL,
  `ApplicationName` VARCHAR(256) NOT NULL,
  `ActiveFlag` BIT NOT NULL,
  `CreatedBy` VARCHAR(128) NOT NULL,
  `CreatedTime` DATETIME NOT NULL,
  `UpdatedBy` VARCHAR(128) NOT NULL,
  `UpdatedTime` DATETIME NOT NULL,
  PRIMARY KEY (`ExternalAPI_PK`))
ENGINE = InnoDB;

CREATE TABLE `api_request` (
  `APIRequest_PK` INT NOT NULL AUTO_INCREMENT,
  `ExternalAPI_FK` INT NOT NULL,
  `RequestID` VARCHAR(64) NOT NULL,-- Unique from external API
  `RequestUser` VARCHAR(256) NOT NULL,
  `RequestStartTime` DATETIME NOT NULL,
  `RequestEndTime` DATETIME NOT NULL,
  PRIMARY KEY (`APIRequest_PK`),
  UNIQUE INDEX `unique_api_request` (`ExternalAPI_FK` ASC, `RequestID` ASC),
  CONSTRAINT `api_request__external_api` FOREIGN KEY (`ExternalAPI_FK`) REFERENCES `external_api` (`ExternalAPI_PK`) ON DELETE NO ACTION ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE `api_request_detail` (
  `RequestDetail_PK` INT NOT NULL AUTO_INCREMENT,
  `APIRequest_FK` INT NOT NULL,
  `WebMethodName` VARCHAR(256) NOT NULL,
  `WebMethodParameters` VARCHAR(2048) NULL,
  `RequestSenderIPAddress` VARCHAR(64) NOT NULL,
  `RequestProcessorIPAddress` VARCHAR(64) NOT NULL,
  `HTTPResponseCode` INT NOT NULL,
  `RequestSize` INT NOT NULL,
  `ResponseSize` INT NOT NULL,
  PRIMARY KEY (`RequestDetail_PK`),
  CONSTRAINT `api_request_detail__api_request` FOREIGN KEY (`APIRequest_FK`) REFERENCES `api_request` (`APIRequest_PK`) ON DELETE NO ACTION ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE `api_request_detail_exception` (
  `RequestException_PK` INT NOT NULL AUTO_INCREMENT,
  `APIRequest_FK` INT NOT NULL,
  `ExceptionMessage` VARCHAR(1024) NOT NULL,
  `ClassName` VARCHAR(256) NOT NULL,
  `ErrorLineNumber` INT NULL,
  PRIMARY KEY (`RequestException_PK`),
  CONSTRAINT `api_request_detail_exception__api_request` FOREIGN KEY (`APIRequest_FK`) REFERENCES `api_request` (`APIRequest_PK`) ON DELETE NO ACTION ON UPDATE NO ACTION)
ENGINE = InnoDB;