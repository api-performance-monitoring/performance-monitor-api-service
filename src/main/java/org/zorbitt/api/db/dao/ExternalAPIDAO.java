package org.zorbitt.api.db.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.zorbitt.api.db.dao.base.AbstractDAO;
import org.zorbitt.api.db.entities.ExternalAPI;

/**
 * 
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ExternalAPIDAO extends AbstractDAO<ExternalAPI, Integer> {

	
	public ExternalAPIDAO(){
		super(ExternalAPI.class);
	}
	/**
	 * Gets an {@link ExternalAPI} by its ApplicationID
	 * @param applicationID	{@link String}
	 * @return externalAPI
	 */
	public ExternalAPI getExternalAPIByApplicationID(String applicationID) {
		return super.selectOne("getExternalAPIByApplicationID", applicationID);
	}
	
}
