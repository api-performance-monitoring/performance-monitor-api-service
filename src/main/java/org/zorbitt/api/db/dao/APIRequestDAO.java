package org.zorbitt.api.db.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.zorbitt.api.db.dao.base.AbstractDAO;
import org.zorbitt.api.db.entities.APIRequest;

/**
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class APIRequestDAO extends AbstractDAO<APIRequest, Integer> {

	public APIRequestDAO(){
		super(APIRequest.class);
	}
	
	public APIRequest getApiRequestByRequestID(Integer externalApiFK, String requestID){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("requestID", requestID);
		map.put("externalApiFK", externalApiFK);
		return selectOne("getApiRequestByRequestID", map);
	}
}
