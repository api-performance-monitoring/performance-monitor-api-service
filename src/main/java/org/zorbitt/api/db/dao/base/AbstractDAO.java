package org.zorbitt.api.db.dao.base;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import static org.zorbitt.api.db.dao.base.SqlSessionFactoryProvider.SESSION_FACTORY_NAME;

/**
 * Abstract DAO class implementing shared code Date Created: 22 Mar 2014
 * 
 * @author Ryan Zakariudakis
 */
public abstract class AbstractDAO<T, PK> implements BasicDAO<T, PK> {
	private static final String NAME_SPACE_SEPERATOR = ".";
	@Inject @Named(SESSION_FACTORY_NAME) protected SqlSession session;
	
	protected Logger log;
	/**
	 * Base namespace for all mappers
	 */
	private String base_namespace = "org.zorbitt.api.db.entities";//Default
	/**
	 * Full namespace for DAO mapper
	 */
	private String namespace = "";
	
	private Class<T> type;
	private String targetClassName;

	// #########
	// Define prefixes for easier naming conventions between XML mapper files and
	// the DAO class
	// #########
	/**
	 * prefix of select queries in mapper
	 */
	public static final String PREFIX_SELECT_QUERY = "get";
	/**
	 * prefix of insert queries in mapper files
	 */
	public static final String PREFIX_INSERT_QUERY = "insert";
	/**
	 * prefix of update queries in mapper files
	 */
	public static final String PREFIX_UPDATE_QUERY = "update";
	/**
	 * prefix of delete queries in mapper files
	 */
	public static final String PREFIX_DELETE_QUERY = "delete"; 
	
	/**
	 * Pre-Configured Constructor
	 * @param targetClassName - {@link String}
	 */
	public AbstractDAO(Class<T> targetClassName) {
		type =  targetClassName;
		this.targetClassName = type.getSimpleName();
		namespace = base_namespace + NAME_SPACE_SEPERATOR +this.targetClassName;
		log = Logger.getLogger(targetClassName+"DAO");
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public T get(PK id) throws PersistenceException {
		String query = namespace + NAME_SPACE_SEPERATOR + PREFIX_SELECT_QUERY + targetClassName;
		T obj = (T) session.selectOne(query, id);
		return obj;
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<T> getAll() throws PersistenceException {
		String query = namespace + NAME_SPACE_SEPERATOR + PREFIX_SELECT_QUERY + "All";
		List<T> list = (List<T>) session.selectList(query);
		return list;
	}
	
	public Integer getTableRowCount(){
		String query = namespace + NAME_SPACE_SEPERATOR + "getTableRowCount";
		Integer totalRows = (Integer) session.selectOne(query);
		return totalRows;
	}
	
	public Boolean hasData(){
		Integer total = getTableRowCount();
		if (total != null && total.intValue() > 0)
			return true;
		return false;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public int insert(T entity) throws PersistenceException {
		String query = namespace + NAME_SPACE_SEPERATOR + PREFIX_INSERT_QUERY + targetClassName;
		Integer status = (Integer) session.insert(query, entity);
		return status;
	}

	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public int update(T entity) throws PersistenceException {
		String query = namespace + NAME_SPACE_SEPERATOR + PREFIX_UPDATE_QUERY + targetClassName;
		Integer status = session.update(query, entity);
		return status;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public int delete(PK pk) throws PersistenceException {
		String query = namespace + NAME_SPACE_SEPERATOR + PREFIX_DELETE_QUERY + targetClassName;
		Integer status = session.delete(query, pk);
		return status;
	}
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	protected T selectOne(String queryName){
		return session.selectOne(namespace + NAME_SPACE_SEPERATOR + queryName);
	}
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	protected T selectOne(String queryName, Object parameter){
		return session.selectOne(namespace + NAME_SPACE_SEPERATOR + queryName, parameter);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	protected List<T> selectList(String queryName){
		return session.selectList(namespace + NAME_SPACE_SEPERATOR + queryName);
	}
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	protected List<T> selectList(String queryName, Object parameter){
		return session.selectList(namespace + NAME_SPACE_SEPERATOR + queryName, parameter);
	}
	
	public String getEntityKeyFromReference(String referenceNumber){
		if (referenceNumber != null){
			//Reference numbers are simply the Primary key of an Entity with some 'salting' to discourage externals storing them.
			String[] refParts = referenceNumber.split("_");//Underscore seperates real PK from rest of the referenceNumber
			if (refParts.length>1){
				String entityPK = refParts[1];
				return entityPK;
			}
			return null;
		}
		return null;
	}
}
