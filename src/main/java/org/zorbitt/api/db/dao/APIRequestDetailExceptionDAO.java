package org.zorbitt.api.db.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.zorbitt.api.db.dao.base.AbstractDAO;
import org.zorbitt.api.db.entities.APIRequestDetailException;

/**
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class APIRequestDetailExceptionDAO extends AbstractDAO<APIRequestDetailException, Integer> {

	public APIRequestDetailExceptionDAO(){
		super(APIRequestDetailException.class);
	}
}
