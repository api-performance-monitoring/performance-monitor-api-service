package org.zorbitt.api.db.dao.base;

import java.io.InputStream;
import java.io.Reader;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;

@Stateless
@Named("MybatisSessionFactoryProvider")
public class SqlSessionFactoryProvider {
	public static final String SESSION_FACTORY_NAME = "mybatis_factory";
    private Logger log = Logger.getLogger(SqlSessionFactoryProvider.class);
    private static final String default_mybatis_environment = "production";
	private static final String default_mybatis_config_file_name = "mybatis-sqlserver-config.xml";
    
    @Produces
    @ApplicationScoped
    @Named(SESSION_FACTORY_NAME)
    public SqlSessionFactory produceFactory() throws Exception {
    	String environment = null;
    	String configFileName = null;
    	Properties props = new Properties();
    	InputStream propsIn = null;
    	
    	propsIn = getClass().getResourceAsStream("/test.application.properties");
		if (propsIn == null){
			log.debug("test.application.properties not found. using application.properties");
			propsIn = getClass().getResourceAsStream("/application.properties");
			if (propsIn == null){
				log.debug("application.properties not found. using default application.properties");
			} else {
				log.debug("application.properties found");
			}
		}else {
			log.debug("test.environment.properties found");
		}
		props.load(propsIn);
    	environment = (String) props.get("mybatis.env");
    	configFileName = (String) props.get("mybatis.configpath");
    	
    	if ((environment != null && !environment.isEmpty()) && (configFileName != null && !configFileName.isEmpty())){
    		return createSessionFactory(configFileName, environment);
    	}
    	return createSessionFactory(default_mybatis_config_file_name, default_mybatis_environment);
    }
	/**
	 * Creates the mybatis session factory
	 * @param configFileName - Filename to Use as config
	 * @return sessionFactory - {@link SqlSessionFactory}
	 * @throws Exception 
	 */
	private SqlSessionFactory createSessionFactory(String configFileName, String environment) throws Exception{
		try {
			log.info("Setting up the SqlSessionFactory using " + configFileName);
			Reader reader = Resources.getResourceAsReader(configFileName);
			SqlSessionFactory sqlSessionFactory = null;
			if (environment == null){
				environment = "default";
				sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
			} else {
				sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader, environment);
			}
			log.debug(sqlSessionFactory.getConfiguration().toString());
			log.info("Successfully set up the SqlSessionFactory using " + configFileName + " and Environment: " + environment);
			return sqlSessionFactory;
		} catch (Exception e) {
			throw new Exception("Could not create SQLSessionFactory. Cause: " + e.getMessage(), e);
		}
	}
}