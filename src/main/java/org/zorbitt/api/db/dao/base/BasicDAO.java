package org.zorbitt.api.db.dao.base;

import java.util.List;

import org.apache.ibatis.exceptions.PersistenceException;

/**
 * Interface defining basic methods needed for a DAO
 * Date Created: 22 Mar 2014
 * @author Ryan Zakariudakis
 */
public interface BasicDAO<T, PK> {

	 public T get(PK id) throws PersistenceException;

	 public List<T> getAll() throws PersistenceException;

	 public int insert(T objInstance) throws PersistenceException;

	 public int update(T transientObject) throws PersistenceException;

	 public int delete(PK id)  throws PersistenceException;

	 public Integer getTableRowCount();

	 public Boolean hasData();

	public String getEntityKeyFromReference(String referenceNumber);
}
