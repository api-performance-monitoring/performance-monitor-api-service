package org.zorbitt.api.db.entities;

import java.util.Date;

/**
 * 
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
public class APIRequest {

	private Integer apiRequestPK;
	private ExternalAPI externalApi;
	private Integer externalApiFK;
	private String requestID;
	private String requestUser;
	private Date requestStartTime;
	private Date requestEndTime;
	/**
	 * @return the apiRequestPK - Integer
	 */
	public Integer getApiRequestPK() {
		return apiRequestPK;
	}
	/**
	 * @param apiRequestPK ({field_type}) the apiRequestPK to set
	 */
	public void setApiRequestPK(Integer apiRequestPK) {
		this.apiRequestPK = apiRequestPK;
	}
	/**
	 * @return the externalApi - ExternalAPI
	 */
	public ExternalAPI getExternalApi() {
		return externalApi;
	}
	/**
	 * @param externalApi ({field_type}) the externalApi to set
	 */
	public void setExternalApi(ExternalAPI externalApi) {
		this.externalApi = externalApi;
	}
	/**
	 * @return the externalApiFK - Integer
	 */
	public Integer getExternalApiFK() {
		return externalApiFK;
	}
	/**
	 * @param externalApiFK ({field_type}) the externalApiFK to set
	 */
	public void setExternalApiFK(Integer externalApiFK) {
		this.externalApiFK = externalApiFK;
	}
	/**
	 * @return the requestID - String
	 */
	public String getRequestID() {
		return requestID;
	}
	/**
	 * @param requestID ({field_type}) the requestID to set
	 */
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	/**
	 * @return the requestUser - String
	 */
	public String getRequestUser() {
		return requestUser;
	}
	/**
	 * @param requestUser ({field_type}) the requestUser to set
	 */
	public void setRequestUser(String requestUser) {
		this.requestUser = requestUser;
	}
	/**
	 * @return the requestStartTime - Date
	 */
	public Date getRequestStartTime() {
		return requestStartTime;
	}
	/**
	 * @param requestStartTime ({field_type}) the requestStartTime to set
	 */
	public void setRequestStartTime(Date requestStartTime) {
		this.requestStartTime = requestStartTime;
	}
	/**
	 * @return the requestEndTime - Date
	 */
	public Date getRequestEndTime() {
		return requestEndTime;
	}
	/**
	 * @param requestEndTime ({field_type}) the requestEndTime to set
	 */
	public void setRequestEndTime(Date requestEndTime) {
		this.requestEndTime = requestEndTime;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "APIRequest [apiRequestPK=" + apiRequestPK + ", externalApi="
				+ externalApi + ", externalApiFK=" + externalApiFK
				+ ", requestID=" + requestID + ", requestUser=" + requestUser
				+ ", requestStartTime=" + requestStartTime
				+ ", requestEndTime=" + requestEndTime + "]";
	}
}
