package org.zorbitt.api.db.entities;

import java.util.Date;

/**
 * 
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
public class ExternalAPI {

	private Integer externalAPIPK;
	private String applicationID;
	private String applicationName;
	private Boolean activeFlag;
	private String createdBy;
	private Date createdTime;
	private String updatedBy;
	private Date updatedTime;
	/**
	 * @return the externalAPIPK - Integer
	 */
	public Integer getExternalAPIPK() {
		return externalAPIPK;
	}
	/**
	 * @param externalAPIPK ({field_type}) the externalAPIPK to set
	 */
	public void setExternalAPIPK(Integer externalAPIPK) {
		this.externalAPIPK = externalAPIPK;
	}
	/**
	 * @return the applicationID - String
	 */
	public String getApplicationID() {
		return applicationID;
	}
	/**
	 * @param applicationID ({field_type}) the applicationID to set
	 */
	public void setApplicationID(String applicationID) {
		this.applicationID = applicationID;
	}
	/**
	 * @return the applicationName - String
	 */
	public String getApplicationName() {
		return applicationName;
	}
	/**
	 * @param applicationName ({field_type}) the applicationName to set
	 */
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	/**
	 * @return the activeFlag - Boolean
	 */
	public Boolean getActiveFlag() {
		return activeFlag;
	}
	/**
	 * @param activeFlag ({field_type}) the activeFlag to set
	 */
	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
	/**
	 * @return the createdBy - String
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy ({field_type}) the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdTime - Date
	 */
	public Date getCreatedTime() {
		return createdTime;
	}
	/**
	 * @param createdTime ({field_type}) the createdTime to set
	 */
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	/**
	 * @return the updatedBy - String
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy ({field_type}) the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the updatedTime - Date
	 */
	public Date getUpdatedTime() {
		return updatedTime;
	}
	/**
	 * @param updatedTime ({field_type}) the updatedTime to set
	 */
	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ExternalAPI [externalAPIPK=" + externalAPIPK
				+ ", applicationID=" + applicationID + ", applicationName="
				+ applicationName + ", activeFlag=" + activeFlag
				+ ", createdBy=" + createdBy + ", createdTime=" + createdTime
				+ ", updatedBy=" + updatedBy + ", updatedTime=" + updatedTime
				+ "]";
	}	
}
