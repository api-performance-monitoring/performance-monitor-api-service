package org.zorbitt.api.db.entities;

/**
 * 
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
public class APIRequestDetail {

	private Integer requestDetailPK;
	private Integer apiRequestFK;
	private APIRequest apiRequest;
	private String webMethodName;
	private String webMethodParameters;
	private String requestSenderIPAddress;
	private String requestProcessorIPAddress;
	private Integer httpResponseCode;
	private Long requestSize;
	private Long responseSize;
	/**
	 * @return the requestDetailPK - Integer
	 */
	public Integer getRequestDetailPK() {
		return requestDetailPK;
	}
	/**
	 * @param requestDetailPK ({field_type}) the requestDetailPK to set
	 */
	public void setRequestDetailPK(Integer requestDetailPK) {
		this.requestDetailPK = requestDetailPK;
	}
	/**
	 * @return the apiRequestFK - Integer
	 */
	public Integer getApiRequestFK() {
		return apiRequestFK;
	}
	/**
	 * @param apiRequestFK ({field_type}) the apiRequestFK to set
	 */
	public void setApiRequestFK(Integer apiRequestFK) {
		this.apiRequestFK = apiRequestFK;
	}
	/**
	 * @return the apiRequest - APIRequest
	 */
	public APIRequest getApiRequest() {
		return apiRequest;
	}
	/**
	 * @param apiRequest ({field_type}) the apiRequest to set
	 */
	public void setApiRequest(APIRequest apiRequest) {
		this.apiRequest = apiRequest;
	}
	/**
	 * @return the webMethodName - String
	 */
	public String getWebMethodName() {
		return webMethodName;
	}
	/**
	 * @param webMethodName ({field_type}) the webMethodName to set
	 */
	public void setWebMethodName(String webMethodName) {
		this.webMethodName = webMethodName;
	}
	/**
	 * @return the webMethodParameters - String
	 */
	public String getWebMethodParameters() {
		return webMethodParameters;
	}
	/**
	 * @param webMethodParameters ({field_type}) the webMethodParameters to set
	 */
	public void setWebMethodParameters(String webMethodParameters) {
		this.webMethodParameters = webMethodParameters;
	}
	/**
	 * @return the requestSenderIPAddress - String
	 */
	public String getRequestSenderIPAddress() {
		return requestSenderIPAddress;
	}
	/**
	 * @param requestSenderIPAddress ({field_type}) the requestSenderIPAddress to set
	 */
	public void setRequestSenderIPAddress(String requestSenderIPAddress) {
		this.requestSenderIPAddress = requestSenderIPAddress;
	}
	/**
	 * @return the requestProcessorIPAddress - String
	 */
	public String getRequestProcessorIPAddress() {
		return requestProcessorIPAddress;
	}
	/**
	 * @param requestProcessorIPAddress ({field_type}) the requestProcessorIPAddress to set
	 */
	public void setRequestProcessorIPAddress(String requestProcessorIPAddress) {
		this.requestProcessorIPAddress = requestProcessorIPAddress;
	}
	/**
	 * @return the httpResponseCode - Integer
	 */
	public Integer getHttpResponseCode() {
		return httpResponseCode;
	}
	/**
	 * @param httpResponseCode ({field_type}) the httpResponseCode to set
	 */
	public void setHttpResponseCode(Integer httpResponseCode) {
		this.httpResponseCode = httpResponseCode;
	}
	/**
	 * @return the requestSize - Long
	 */
	public Long getRequestSize() {
		return requestSize;
	}
	/**
	 * @param requestSize ({field_type}) the requestSize to set
	 */
	public void setRequestSize(Long requestSize) {
		this.requestSize = requestSize;
	}
	/**
	 * @return the responseSize - Long
	 */
	public Long getResponseSize() {
		return responseSize;
	}
	/**
	 * @param responseSize ({field_type}) the responseSize to set
	 */
	public void setResponseSize(Long responseSize) {
		this.responseSize = responseSize;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "APIRequestDetail [requestDetailPK=" + requestDetailPK
				+ ", apiRequestFK=" + apiRequestFK + ", apiRequest="
				+ apiRequest + ", webMethodName=" + webMethodName
				+ ", webMethodParameters=" + webMethodParameters
				+ ", requestSenderIPAddress=" + requestSenderIPAddress
				+ ", requestProcessorIPAddress=" + requestProcessorIPAddress
				+ ", httpResponseCode=" + httpResponseCode + ", requestSize="
				+ requestSize + ", responseSize=" + responseSize + "]";
	}
}
