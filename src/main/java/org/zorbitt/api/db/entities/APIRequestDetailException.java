package org.zorbitt.api.db.entities;

/**
 * 
 * 
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
public class APIRequestDetailException {

	private Integer requestExceptionPK;
	private Integer apiRequestFK;
	private APIRequest apiRequest;
	private String exceptionMessage;
	private String className;
	private Integer errorLineNumber;
	/**
	 * @return the requestExceptionPK - Integer
	 */
	public Integer getRequestExceptionPK() {
		return requestExceptionPK;
	}
	/**
	 * @param requestExceptionPK ({field_type}) the requestExceptionPK to set
	 */
	public void setRequestExceptionPK(Integer requestExceptionPK) {
		this.requestExceptionPK = requestExceptionPK;
	}
	/**
	 * @return the apiRequestFK - Integer
	 */
	public Integer getApiRequestFK() {
		return apiRequestFK;
	}
	/**
	 * @param apiRequestFK ({field_type}) the apiRequestFK to set
	 */
	public void setApiRequestFK(Integer apiRequestFK) {
		this.apiRequestFK = apiRequestFK;
	}
	/**
	 * @return the apiRequest - APIRequest
	 */
	public APIRequest getApiRequest() {
		return apiRequest;
	}
	/**
	 * @param apiRequest ({field_type}) the apiRequest to set
	 */
	public void setApiRequest(APIRequest apiRequest) {
		this.apiRequest = apiRequest;
	}
	/**
	 * @return the exceptionMessage - String
	 */
	public String getExceptionMessage() {
		return exceptionMessage;
	}
	/**
	 * @param exceptionMessage ({field_type}) the exceptionMessage to set
	 */
	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}
	/**
	 * @return the className - String
	 */
	public String getClassName() {
		return className;
	}
	/**
	 * @param className ({field_type}) the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	/**
	 * @return the errorLineNumber - Integer
	 */
	public Integer getErrorLineNumber() {
		return errorLineNumber;
	}
	/**
	 * @param errorLineNumber ({field_type}) the errorLineNumber to set
	 */
	public void setErrorLineNumber(Integer errorLineNumber) {
		this.errorLineNumber = errorLineNumber;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "APIRequestDetailException [requestExceptionPK="
				+ requestExceptionPK + ", apiRequestFK=" + apiRequestFK
				+ ", apiRequest=" + apiRequest + ", exceptionMessage="
				+ exceptionMessage + ", className=" + className + ", errorLineNumber="
				+ errorLineNumber + "]";
	}
}
