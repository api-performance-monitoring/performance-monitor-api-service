package org.zorbitt.api.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.log4j.Logger;
import org.zorbitt.api.ApiCallException;
import org.zorbitt.api.handler.APIRequestHandler;

/**
 * 
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
@MessageDriven(name = "APIRequestExceptionMDB", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = APIRequestJMS.APIREQUESTEXCEPTION_QUEUE),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") })
public class APIRequestExceptionMDB implements MessageListener{

	private Logger logger = Logger.getLogger(APIRequestExceptionMDB.class);
	
	@EJB APIRequestHandler apiRequestHandler;
	/* (non-Javadoc)
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	public void onMessage(Message msg) {
		ApiCallException apiCallException = null;
		try {
			apiCallException = msg.getBody(ApiCallException.class);
		} catch (JMSException e) {
			logger.error(e.getMessage(),e);
		}
		apiRequestHandler.handleAPIRequestException(apiCallException);
	}

}
