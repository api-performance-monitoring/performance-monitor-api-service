package org.zorbitt.api.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.log4j.Logger;
import org.zorbitt.api.ApiCall;
import org.zorbitt.api.handler.APIRequestHandler;

/**
 * 
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
@MessageDriven(name = "APIRequestMDB", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = APIRequestJMS.APIREQUEST_QUEUE),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") })
public class APIRequestMDB implements MessageListener{

	private Logger logger = Logger.getLogger(APIRequestMDB.class);
	
	@EJB APIRequestHandler apiRequestHandler;
	/* (non-Javadoc)
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	public void onMessage(Message msg) {
		
		ApiCall apiCall = null;
		try {
			apiCall = msg.getBody(ApiCall.class);
		} catch (JMSException e) {
			logger.error(e.getMessage(),e);
		}
		apiRequestHandler.handleAPIRequest(apiCall);
	}

}
