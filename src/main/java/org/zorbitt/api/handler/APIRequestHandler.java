package org.zorbitt.api.handler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.zorbitt.api.ApiCall;
import org.zorbitt.api.ApiCallException;
import org.zorbitt.api.db.dao.APIRequestDAO;
import org.zorbitt.api.db.dao.APIRequestDetailDAO;
import org.zorbitt.api.db.dao.APIRequestDetailExceptionDAO;
import org.zorbitt.api.db.dao.ExternalAPIDAO;
import org.zorbitt.api.db.entities.APIRequest;
import org.zorbitt.api.db.entities.APIRequestDetail;
import org.zorbitt.api.db.entities.APIRequestDetailException;
import org.zorbitt.api.db.entities.ExternalAPI;

/**
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class APIRequestHandler {
	
	Logger log = Logger.getLogger(APIRequestHandler.class);
	@EJB ExternalAPIDAO externalAPIDAO;
	@EJB APIRequestDAO apiRequestDAO;
	@EJB APIRequestDetailDAO apiRequestDetailDAO;
	@EJB APIRequestDetailExceptionDAO apiRequestDetailExceptionDAO;
	
	public void handleAPIRequest(ApiCall apiCall){
		ExternalAPI api = externalAPIDAO.getExternalAPIByApplicationID(apiCall.getApplicationID());
		if (api != null ) {
			APIRequest apiReq = apiRequestDAO.getApiRequestByRequestID(api.getExternalAPIPK(), apiCall.getRequestID());
			if (apiReq == null ) {
				APIRequest apiRequest = new APIRequest();
				apiRequest.setExternalApiFK(api.getExternalAPIPK());
				apiRequest.setRequestID(apiCall.getRequestID());
				apiRequest.setRequestUser(apiCall.getRequestUser());
				apiRequest.setRequestStartTime(new Date(apiCall.getRequestStartTime()));
				apiRequest.setRequestEndTime(new Date(apiCall.getRequestEndTime()));
				apiRequestDAO.insert(apiRequest);
				if (apiCall.getApiCallMetaData() != null) {
					APIRequestDetail apiRequestDetail = new APIRequestDetail();
					apiRequestDetail.setApiRequestFK(apiRequest.getApiRequestPK());
					apiRequestDetail.setHttpResponseCode(apiCall.getApiCallMetaData().getHttpResponseCode());
					apiRequestDetail.setRequestProcessorIPAddress(apiCall.getApiCallMetaData().getRequestProcessorIPAddress());
					apiRequestDetail.setRequestSenderIPAddress(apiCall.getApiCallMetaData().getRequestSenderIPAddress());
					apiRequestDetail.setRequestSize(apiCall.getApiCallMetaData().getRequestSize());
					apiRequestDetail.setResponseSize(apiCall.getApiCallMetaData().getResponseSize());
					String url = apiCall.getApiCallMetaData().getWebRequestURL();
					String[] urlParts = StringUtils.split(url, "?");
					if (urlParts != null && urlParts.length>0){
						apiRequestDetail.setWebMethodName(urlParts[0]);
						if (urlParts.length>1){
							String urlParams = "";
							for (int i=1;i<urlParts.length;i++){
								urlParams += urlParts[i];
							}
							apiRequestDetail.setWebMethodParameters(urlParams);
						}
					}
					apiRequestDetailDAO.insert(apiRequestDetail);
				}
			} else {
				log.error("Request Already processed");
			}
			
		} else {
			log.error("Error handling request for APICall Object: \n"+apiCall);
		}
	}
	
	public void handleAPIRequestException(ApiCallException apiCallException) {
		ExternalAPI api = externalAPIDAO.getExternalAPIByApplicationID(apiCallException.getApplicationID());
		if (api != null ) {
			APIRequest apiReq = apiRequestDAO.getApiRequestByRequestID(api.getExternalAPIPK(), apiCallException.getRequestID());
			if (apiReq != null ) {
				APIRequestDetailException requestDetailException = new APIRequestDetailException();
				requestDetailException.setApiRequestFK(apiReq.getApiRequestPK());
				requestDetailException.setErrorLineNumber(apiCallException.getErrorLineNumber());
				requestDetailException.setClassName(apiCallException.getClassName());
				requestDetailException.setExceptionMessage(apiCallException.getExceptionMessage());
				apiRequestDetailExceptionDAO.insert(requestDetailException);
			} else {
				log.error("Error handling request for APICall Object: \n"+apiCallException);
			}
		} else {
			log.error("Error handling request for APICall Object: \n"+apiCallException);
		}
		
	}
}
