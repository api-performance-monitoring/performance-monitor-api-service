package org.zorbitt.api.rest;

import java.util.HashMap;
import java.util.Map;

/**
 * Class used to store meta-data for shared-service ws in Memory
 * <br><b>Date Created</b>: 06 Oct 2015
 * @author ryan.zakariudakis
 */
public class ApplicationInfoVO {

	public static final String JenkinsBuildTag = "Jenkins-Build-Tag";
	public static final Map<String, String> appInfo = new HashMap<String, String>();
		
}