package org.zorbitt.api.rest.endpoints;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.zorbitt.api.ApiCall;
import org.zorbitt.api.ApiCallException;
import org.zorbitt.api.handler.APIRequestHandler;
import org.zorbitt.api.rest.IPerformanceMonitor;

/**
 * Rest Resource for {@link IPerformanceMonitor}
 * <br><b>Date Created</b>: 27 May 2015
 * @author ryan.zakariudakis
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class PerformanceMonitorResource implements IPerformanceMonitor {
	private Logger log = Logger.getLogger(PerformanceMonitorResource.class);
	@EJB APIRequestHandler apiRequestHandler;
	@Inject ServletContext context;

	/* (non-Javadoc)
	 * @see org.zorbitt.api.rest.IPerformanceMonitor#submit(org.zorbitt.api.ApiCall)
	 */
	public Boolean submit(ApiCall apiCall) {
		try {
			processSubmission(apiCall);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			return false;
		}
	}
	/* (non-Javadoc)
	 * @see org.zorbitt.api.rest.IPerformanceMonitor#submitException(org.zorbitt.api.ApiCallException)
	 */
	public Boolean submitException(ApiCallException apiCallException) {
		try {
			processExceptionSubmission(apiCallException);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			return false;
		}
	}
	@Asynchronous
	private void processSubmission(ApiCall apiCall) {
		apiRequestHandler.handleAPIRequest(apiCall);
	}
	@Asynchronous
	private void processExceptionSubmission(ApiCallException apiCallException) {
		apiRequestHandler.handleAPIRequestException(apiCallException);
	}
}