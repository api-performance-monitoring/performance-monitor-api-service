package org.zorbitt.api.rest.providers;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map.Entry;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.servlet.ServletContext;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;
import org.zorbitt.api.rest.ApplicationInfoVO;

/**
 * Post Container Response Filter, used to add extra headers to all responses.
 * <br>
 * <b>Date Created</b>: 20 Aug 2015
 * 
 * @author ryan.zakariudakis
 */
@Provider
public class PostContainerResponseFilter implements ContainerResponseFilter {

	private @Context ServletContext servletContext;
	private Logger logger = Logger.getLogger(PostContainerResponseFilter.class);
	private static final String manifest_path = "/META-INF/MANIFEST.MF";
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.ws.rs.container.ContainerResponseFilter#filter(javax.ws.rs.container
	 * .ContainerRequestContext, javax.ws.rs.container.ContainerResponseContext)
	 */
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
		getManifestInfo();
		responseContext.getHeaders().add("Access-Control-Allow-Origin", "*");
		responseContext.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
		responseContext.getHeaders().add("Access-Control-Allow-Credentials", "true");
		responseContext.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
		responseContext.getHeaders().add("Access-Control-Max-Age", "1209600");
		if (!responseContext.hasEntity()){
			responseContext.getHeaders().add("ResponseInfo", "NoResults");
		} else {
			responseContext.getHeaders().add("ResponseInfo", "Success");
		}
		//Add extra meta data to response headers
		for(Entry<String, String> curSet : ApplicationInfoVO.appInfo.entrySet()){
			responseContext.getHeaders().add(curSet.getKey(), curSet.getValue());
		}
	}
	
	private void getManifestInfo(){
		try {
			if (ApplicationInfoVO.appInfo.containsValue(ApplicationInfoVO.JenkinsBuildTag))//Only get details once
				return;
			InputStream inputStream = servletContext.getResourceAsStream(manifest_path);
			Manifest manifest = new Manifest(inputStream);
			Attributes attributes = manifest.getMainAttributes();
			String buildTag = attributes.getValue(ApplicationInfoVO.JenkinsBuildTag);
			ApplicationInfoVO.appInfo.put(ApplicationInfoVO.JenkinsBuildTag, buildTag);
		} catch (IOException e) {
			logger.error("Cannot get " + manifest_path);
		}
	}
}
