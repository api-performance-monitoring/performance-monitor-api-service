package org.zorbitt.api.rest;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.zorbitt.api.rest.endpoints.PerformanceMonitorResource;

/**
 * Extends the {@link Application} to setup the root of the REST service
 * @author ryan.zakariudakis
 * <br>Created Date: 20 April 2015
 */
@ApplicationPath("v1/ws")
public class PerformanceMonitorApplication extends Application {

	/*
	 * ############
	 * Version History
	 * ############
	 * 1 - Initial Version
	 * 
	 */
	public PerformanceMonitorApplication(){
		BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1");
        beanConfig.setSchemes(new String[]{"http"});
        String hostName = System.getenv("COMPUTERNAME");
        if (hostName == null || hostName.isEmpty())
        	hostName = "localhost";
        hostName += ":8080";
        beanConfig.setHost(hostName.toLowerCase());
        beanConfig.setBasePath("/performancemonitor/v1/ws/");
        beanConfig.setDescription("API Performance Monitoring Service");
        beanConfig.setPrettyPrint(true);
        beanConfig.setTitle("API Performance Monitoring");
        beanConfig.setResourcePackage(this.getClass().getPackage().getName());
        beanConfig.setScan(true);
	}
	@Override
	public Set<Class<?>> getClasses() {
        // set your resources here
		Set<Class<?>> resources = new HashSet<Class<?>>();
		resources.add(ApiListingResource.class);
		resources.add(SwaggerSerializers.class);
		resources.add(PerformanceMonitorResource.class);
		return resources;
    }
	
	
}
