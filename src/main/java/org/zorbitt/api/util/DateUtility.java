package org.zorbitt.api.util;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.ejb.Stateless;

import org.apache.log4j.Logger;

/**
 * Date Utils helper class for handy date methods
 * <br><br><b>Date Created</b>: 18 Jun 2012
 * @author ryan.zakariudakis
 **/
@Stateless
public class DateUtility {
	private Logger logger = Logger.getLogger(DateUtility.class);
	/**
	 * Gets Current Date as {@link java.util.Date}
	 * @return currentDate - {@link java.util.Date}
	 */
	public static Date getCurrentDate()
	{		
		return new Date();
	}
	
	/**
	 * Gets Current Date as {@link java.sql.Date} Type 
	 * @return currentDate - {@link java.sql.Date}
	 */
	public static java.sql.Date getCurrentSQLDate()
	{		
		return new java.sql.Date(Calendar.getInstance().getTimeInMillis());
	}
	
	/**
	 * Gets the current Time
	 * @return currentTime - {@link Time}
	 */
	public static Time getCurrentTime()
	{	
		Time s = new Time(Calendar.getInstance().getTimeInMillis());
		return s;
	}
	
	/**
	 * Gets Display date for the client side<br> 
	 * on the WEB PAGE
	 * @param dateToReturn - {@link Date}
	 * @return "EEEEEEEE, dd MMMMM"  see {@link String}
	 */
	public String getWebDisplayDate(Date dateToReturn)
	{
		String dateString = null;
		if(dateToReturn != null)
		{
			dateString = DateFormats.WEEKDAYDATEYEAR_FORMAT.format(dateToReturn);
		}else{
			dateString = "";
		}
		return dateString;
	}
	/**
	 * Converts a Date to basic format
	 * @param dateToConvert  - {@link Date}
	 * @return "dd-MM-yyyy" see {@link String}
	 */
	public String convertToBasicDate(Date dateToConvert)
	{
		String dateString = null;		
		if(dateToConvert != null){
			dateString = DateFormats.DAYMONTHYEAR_FORMAT.format(dateToConvert);
		}
		else{
			dateString = "";
		}
		return dateString;
	}
	/**
	 * Converts a Date to basic XML Date String
	 * @param dateToConvert  - {@link Date}
	 * @return "yyy-MM-dd" see {@link String}
	 */
	public String convertToBasicXMLDateString(Date dateToConvert)
	{
		String dateString = null;		
		if(dateToConvert != null){
			dateString = DateFormats.YEARMONTHDAY_FORMAT.format(dateToConvert);
		}
		else{
			dateString = "";
		}
		return dateString;
	}
	/**
	 * Converts a Date to basic format
	 * @param dateToConvert  - {@link Date}
	 * @return "dd-MM-yyyy" see {@link String}
	 */
	public static String convertToSimpleDate(Date dateToConvert)
	{
		String dateString = null;		
		if(dateToConvert != null){
			dateString = DateFormats.DAYMONTHYEAR_FORMAT.format(dateToConvert);
		}
		else{
			dateString = "";
		}
		return dateString;
	}
	/**
	 * Converts a Date to basic format
	 * @param dateToConvert  - {@link Date}
	 * @return "yyyy-MM-dd hh:mm:ss" see {@link String}
	 */
	public static String convertToDateTime(Date dateToConvert)
	{
		String dateString = null;		
		if(dateToConvert != null){
			dateString = DateFormats.TIMESTAMP_FORMAT.format(dateToConvert);
		}
		else{
			dateString = "";
		}
		return dateString;
	}
	/**
	 * Converts a Date to basic format
	 * @param dateToConvert  - {@link Date}
	 * @return "HH:mm:ss" see {@link String}
	 */
	public static String convertToTime(Date dateToConvert)
	{
		String dateString = null;
		if(dateToConvert != null){
			dateString = DateFormats.TIME_FORMAT.format(dateToConvert);
		}
		else{
			dateString = "";
		}
		return dateString;
	}
	/**
	 * Get a specific date
	 * @param day of month
	 * @param month of year
	 * @param year
	 * @return Date - {@link Date}
	 */
	public Date createDate(int day, int month, int year)
	{
		Calendar curCal = Calendar.getInstance();
		curCal.set(Calendar.YEAR, year);
		curCal.set(Calendar.MONTH, month-1);
		curCal.set(Calendar.DAY_OF_MONTH, day);
		curCal.set(Calendar.HOUR_OF_DAY, 0);
		curCal.set(Calendar.MINUTE, 0);
		curCal.set(Calendar.SECOND, 0);
		curCal.set(Calendar.MILLISECOND, 0);
		return curCal.getTime();
	}
	/**
	 * Get a specific date
	 * @param day
	 * @param month
	 * @param year
	 * @param hourOfDay
	 * @param minuteOfHour
	 * @param secondOfMinute
	 * @return Date - {@link Date}
	 */
	public Date createDate(int day, int month, int year, int hourOfDay, int minuteOfHour, int secondOfMinute)
	{
		Calendar curCal = Calendar.getInstance();
		curCal.set(Calendar.YEAR, year);
		curCal.set(Calendar.MONTH, month-1);
		curCal.set(Calendar.DAY_OF_MONTH, day);
		curCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
		curCal.set(Calendar.MINUTE, minuteOfHour);
		curCal.set(Calendar.SECOND, secondOfMinute);
		curCal.set(Calendar.MILLISECOND, 0);
		return curCal.getTime();
	}
	/**
	 * Get a specific date
	 * @param day - {@link String}
	 * @param month - {@link String}
	 * @param year - {@link String}
	 * @return Date - {@link Date}
	 */
	public Date createDate(String day, String month, String year)
	{
		Calendar curCal = Calendar.getInstance();
		if (year != null && !year.isEmpty())
			curCal.set(Calendar.YEAR, Integer.valueOf(year));
		curCal.set(Calendar.MONTH, Integer.valueOf(month)-1);
		curCal.set(Calendar.DAY_OF_MONTH, Integer.valueOf(day));
		curCal.set(Calendar.HOUR_OF_DAY, 0);
		curCal.set(Calendar.MINUTE, 0);
		curCal.set(Calendar.SECOND, 0);
		curCal.set(Calendar.MILLISECOND, 0);
		return curCal.getTime();
	}
	/**
	 * Gets the current Year as yyyy
	 * @return yyyy see {@link String}
	 */
	public String getCurrentYear()
	{
			
		return DateFormats.FULLYEAR_FORMAT.format(new Date());
	}
	/**
	 * Gets current Timestamp
	 * @return yyyy-MM-dd HH:mm:ss see {@link String}
	 */
	public String getCurrentTimeStamp()
	{
		
		return DateFormats.TIMESTAMP_FORMAT.format(new Date());
	}
	/**
	 * Gets current Timestamp
	 * @return yyyyMMdd(HHmmss.S) see {@link DateFormats}.FILE_TIMESTAMP_FORMAT
	 */
	public static String getCurrentFileTimeStamp()
	{	
		return DateFormats.FILE_TIMESTAMP_FORMAT.format(new Date());
	}
	/**
	 * Gets current Timestamp
	 * @return yyyy-MM-dd HH:mm:ss see {@link String}
	 */
	public String getCurrentFileDate()
	{	
		return DateFormats.FILE_DATE_FORMAT.format(new Date());
	}
	/**
	 * Converts a Date to a timestamp
	 * @param dateToConvert - {@link Date}
	 * @return "yyyy-MM-dd HH:mm:ss" see {@link String}
	 */
	public String convertDateToTimeStamp(Date dateToConvert)
	{
		String dateString = null;
		if(dateToConvert != null){
			dateString = DateFormats.TIMESTAMP_FORMAT.format(dateToConvert);
		}
		else{
			dateString = "";
		}
		return dateString;
	}
	/**
	 * Converts a Date to a timestamp
	 * @param date - {@link Date}
	 * @return sqlDate - {@link java.sql.Date}
	 */
	public java.sql.Date convertDateToSQLDate(Date date)
	{
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		return sqlDate;
	}
	/**
	 * Converts a Date to a timestamp
	 * @param sqlDate - {@link java.sql.Date}
	 * @return date - {@link Date}
	 */
	public Date convertSQLDateToDate(java.sql.Date sqlDate)
	{
		Date date = new Date(sqlDate.getTime());
		return date;
	}
	/**
	 * Adds time to a specific date
	 * @param curDate - {@link Date}
	 * @param days
	 * @param months
	 * @param years
	 * @return newDate - {@link Date}
	 */
	public Date addTimeToDate(Date date, int days, int months, int years )
	{
		Calendar curCal = Calendar.getInstance();
		curCal.setTime(date);
		if(days>0){curCal.add(Calendar.DAY_OF_MONTH, days);}
		if(months>0){curCal.add(Calendar.MONTH, months);}
		if(years>0){curCal.add(Calendar.YEAR, years);}
		return curCal.getTime();
	}
	/**
	 * Adds time to a specific date
	 * @param curDate - {@link Date}
	 * @param days
	 * @param months
	 * @param years
	 * @param hours
	 * @param minutes
	 * @param seconds
	 * @return newDate - {@link Date}
	 */
	public Date addTimeToDate(Date date, int days, int months, int years, int hours, int minutes, int seconds)
	{
		Calendar curCal = Calendar.getInstance();
		curCal.setTime(date);
		if(days>0)curCal.add(Calendar.DAY_OF_MONTH, days);
		if(months>0)curCal.add(Calendar.MONTH, months);
		if(years>0)curCal.add(Calendar.YEAR, years);
		if(hours>0)curCal.add(Calendar.HOUR_OF_DAY, hours);
		if(minutes>0)curCal.add(Calendar.MINUTE, minutes);
		if(seconds>0)curCal.add(Calendar.SECOND, seconds);
		return curCal.getTime();
	}
	/**
	 * Removes time from a specific date
	 * @param curDate - {@link Date}
	 * @param days
	 * @param months
	 * @param years
	 * @return newDate - {@link Date}
	 */
	public Date removeTimeFromDate(Date date, int days, int months, int years )
	{
		Calendar curCal = Calendar.getInstance();
		curCal.setTime(date);
		if(days>0){curCal.add(Calendar.DAY_OF_MONTH, -days);}
		if(months>0){curCal.add(Calendar.MONTH, -months);}
		if(years>0){curCal.add(Calendar.YEAR, -years);}
		return curCal.getTime();
	}
	/**
	 * Removes time from a specific date
	 * @param date - {@link Date}
	 * @param days
	 * @param months
	 * @param years
	 * @param hours
	 * @param minutes
	 * @param seconds
	 * @return newDate - {@link Date}
	 */
	public Date removeTimeFromDate(Date date, int days, int months, int years, int hours, int minutes, int seconds)
	{
		Calendar curCal = Calendar.getInstance();
		curCal.setTime(date);
		if(days>0)curCal.add(Calendar.DAY_OF_MONTH, -days);
		if(months>0)curCal.add(Calendar.MONTH, -months);
		if(years>0)curCal.add(Calendar.YEAR, -years);
		if(hours>0)curCal.add(Calendar.HOUR_OF_DAY, -hours);
		if(minutes>0)curCal.add(Calendar.MINUTE, -minutes);
		if(seconds>0)curCal.add(Calendar.SECOND, -seconds);
		return curCal.getTime();
	}
	/**
	 * Removes time from a specific date
	 * @param dateFrom - {@link Date} date to subtract
	 * @param dateTo - {@link Date} date to subtract from
	 * @return minutesDifference- {@link Integer}
	 */
	public int getDateDifference(Date dateFrom, Date dateTo)
	{		
		int dateDiff = (int)dateTo.getTime() - (int)dateFrom.getTime();
		return ((dateDiff)*1000)*60;
	}
	/**
	 * Parses a Date string into a timestamp date: yy-MM-dd HH:mm:ss
	 * @param date - {@link String}
	 * @return newDate - {@link Date}
	 */
	public Date createDateFromTimeStamp(String dateTimeStamp)
	{
		try{
			DateFormat dateFormat = new SimpleDateFormat("y-MM-dd HH:mm:ss");
			return dateFormat.parse(dateTimeStamp);
		}catch(ParseException e){
			logger.error(e.getMessage(),e);
			return null;
		}
	}
	
	/**
	 * Parses a Date string into a simple date: y-MM-dd
	 * @param dateString - {@link String}
	 * @return newDate - {@link Date}
	 */
	public Date createSimpleDateFromString(String dateString)
	{
		try{
			DateFormat dateFormat = new SimpleDateFormat("y-MM-dd");
			return dateFormat.parse(dateString);
		}catch(ParseException e){
			logger.error(e.getMessage(),e);
			return null;
		}
	}
	
	/**
	 * Parses a Date string into a simple date: y-MM-dd
	 * @param dateString - {@link String} yyyyddmm
	 * @return newDate - {@link Date}
	 */
	public Date parseSimpleDateString(String dateString)
	{
		String year = dateString.substring(0,4);		
		String day = dateString.substring(4,6);
		String month = dateString.substring(6,8);
		String dateToParse = year+"-"+day+"-"+month;
		try{
			DateFormat dateFormat = new SimpleDateFormat("y-MM-dd");
			return dateFormat.parse(dateToParse);
		}catch(ParseException e){
			logger.error(e.getMessage(),e);
			return null;
		}
	}
	/**
	 * Parses a Date String into a {@link Date} from supplied format
	 * @param dateString - {@link String} 
	 * @param datePattern - {@link String} eg: yyyyddmm
	 * @return newDate - {@link Date}
	 */
	public Date parseDate(String dateString, String datePattern) throws ParseException {
		if (dateString == null)
			return null;
		if (datePattern == null)
			return null;
		//Clean String of any excluded characters
		//Check for dashes (-)
		if (!datePattern.contains("-"))
			dateString = dateString.replace("-", "");
		//Check for forward slash (/)
		if (!datePattern.contains("/"))
			dateString = dateString.replace("/", "");
		//Check for slash (\)
		if (!datePattern.contains("\\"))
			dateString = dateString.replace("\\", "");
		if (dateString.length()>datePattern.length())
			dateString = dateString.substring(0, datePattern.length());
		DateFormat dateFormat = new SimpleDateFormat(datePattern);
		return dateFormat.parse(dateString);
	}
	/**
	 * Parses a Date String into a {@link Date} from supplied format
	 * @param dateString - {@link String} 
	 * @param datePattern - {@link String} eg: yyyyddmm
	 * @return newDate - {@link Date}
	 */
	public Date parseDateString(String dateString, String datePattern) throws ParseException {
		String year = dateString.substring(0,4);		
		String day = dateString.substring(4,6);
		String month = dateString.substring(6,8);
		String dateToParse = year+"-"+day+"-"+month;
		logger.debug("Date to parse: "+dateToParse);
		DateFormat dateFormat = new SimpleDateFormat(datePattern);
		return dateFormat.parse(dateToParse);
	}
	/**
	 * Checks if a Time is after another
	 * @param comparator - {@link Time} - Date to check if it is greater than the other
	 * @param compareTo - {@link Time} - Date to compare to
	 * @return result - {@link Boolean}
	 */
	@SuppressWarnings("deprecation")
	public boolean after(Time comparator, Time compareTo){
		if (comparator.getHours() > compareTo.getHours()){
			return true;
		}else if (comparator.getHours() < compareTo.getHours()){
			return false;
		}else if(comparator.getMinutes() > compareTo.getMinutes()){
			return true;
		}else if(comparator.getMinutes() < compareTo.getMinutes()){
			return false;
		}else if (comparator.getSeconds() > compareTo.getSeconds()){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * Checks if a Time is before another
	 * @param comparator - {@link Time} - Date to check if it is less than the other
	 * @param compareTo - {@link Time} - Date to compare to
	 * @return result - {@link Boolean}
	 */
	@SuppressWarnings("deprecation")
	public boolean before(Time comparator, Time compareTo){
		if (comparator.getHours() < compareTo.getHours()){
			return true;
		}else if (comparator.getHours() > compareTo.getHours()){
			return false;
		}else if(comparator.getMinutes() < compareTo.getMinutes()){
			return true;
		}else if(comparator.getMinutes() > compareTo.getMinutes()){
			return false;
		}else if (comparator.getSeconds() < compareTo.getSeconds()){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Parses a Date or Time in Long to a duration in Hours, Minutes and Seconds
	 * @param durationMillis	{@link Long}
	 * @return duration	{@link String}	ResultFormat = {0} Hours, {0} Minutes, {0} Seconds
	 */
	public String parseLongToDuration(long durationMillis){
		long seconds = 0;
		long minutes = 0;
		long hours = Math.round(new Double(durationMillis/3600000));
		long hoursRemainder = durationMillis%3600000;//Remainder of Hours
		minutes = Math.round(new Double(hoursRemainder/60000));
		long minutesRemainder = hoursRemainder%60000;//Remainder of Minutes
		seconds= minutesRemainder/1000;
		return hours + " Hours, " + minutes + " Minutes, " + seconds + " Seconds";
	}
}
