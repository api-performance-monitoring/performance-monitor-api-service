package org.zorbitt.api.util;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.Validate;

/**
 * Class for centralizing Date Formats needed <br/>
 * for GWT front end Date Picker Widgets and <br/>
 * General Date Formatting.
 * <br><br><b>Date Created</b>: 13 Aug 2012
 * @author ryan.zakariudakis
 **/
public class DateFormats implements Serializable{
	private static final long serialVersionUID = 1L;
	/**
	 * Date Format: <b>dd-MM-yyyy</b><br>
	 * Example: <b>28-01-2012</b>
	 */
	public static final DateFormat DAYMONTHYEAR_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
	/**
	 * Date Format: <b>yyyy-MM-dd</b><br>
	 * Example: <b>2012-01-28</b>
	 */
	public static final DateFormat YEARMONTHDAY_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	/**
	 * Date Format: <b>EEEEEEEE, dd MMMMM</b><br>
	 * Example: <b>Tuesday, 10 May</b>
	 */
	public static final DateFormat WEEKDAYDATEYEAR_FORMAT = new SimpleDateFormat("EEEEEEEE, dd MMMMM");
	/**
	 * Date Format: <b>yyyy</b><br>
	 * Example: <b>2012</b>
	 */
	public static final DateFormat FULLYEAR_FORMAT = new SimpleDateFormat("yyyy");
	/**
	 * Date Format: <b>HH:mm:ss</b><br>
	 * Example: <b>23:59:59</b>
	 */
	public static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");
	/**
	 * Date Format: <b>yyyy-MM-dd HH:mm:ss</b><br>
	 * Example: <b>2012-04-30 23:59:59</b>
	 */
	public static final DateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/**
	 * Date Format: <b>yyyyMMdd(HHmmss.S)</b><br>
	 * Example: <b>20120430(235959.000)</b>
	 */
	public static final DateFormat FILE_TIMESTAMP_FORMAT = new SimpleDateFormat("yyyyMMdd(HHmmss.S)");
	/**
	 * Date Format: <b>yyyyMMdd</b><br>
	 * Example: <b>20120430</b>
	 */
	public static final DateFormat FILE_DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
	/**
	 * Formats the Date supplied based on the format.
	 * @param dateFormat - {@link DateFormat}
	 * @param date - {@link Date}
	 * @return dateString - {@link String}
	 */
	public static String formatDate(DateFormat dateFormat, Date date){
		Validate.notNull(dateFormat, "DateFormat Should not be null");
		Validate.notNull(date, "Date Should not be null");
		return dateFormat.format(date);
	}
}
